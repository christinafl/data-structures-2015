package HW1;

/**
 * LinkedList class implements a doubly-linked list. Adapted from Weiss, Data
 * sSructures and Algorithm Analysis in Java. 3rd ed.
 * http://users.cis.fiu.edu/~weiss/dsaajava3/code/MyLinkedList.java
 */
public class MyLinkedList<AnyType> implements Iterable<AnyType> {

	private int theSize;
	private int modCount = 0;
	private Node<AnyType> beginMarker;
	private Node<AnyType> endMarker;

	/**
	 * This is the doubly-linked list node.
	 */
	private static class Node<AnyType> {
		public Node(AnyType d, Node<AnyType> p, Node<AnyType> n) {
			data = d;
			prev = p;
			next = n;
		}

		public AnyType data;
		public Node<AnyType> prev;
		public Node<AnyType> next;
	}

	/**
	 * Construct an empty LinkedList.
	 */
	public MyLinkedList() {
		doClear();
	}

	/**
	 * Change the size of this collection to zero by initializing the beginning
	 * and end marker.
	 */
	public void doClear() {
		beginMarker = new Node<>(null, null, null);
		endMarker = new Node<>(null, beginMarker, null);
		beginMarker.next = endMarker;

		theSize = 0;
		modCount++;
	}

	/**
	 * @return the number of items in this collection.
	 */
	public int size() {
		return theSize;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Gets the Node at position idx, which must range from lower to upper.
	 * 
	 * @param idx
	 *            index to search at.
	 * @param lower
	 *            lowest valid index.
	 * @param upper
	 *            highest valid index.
	 * @return internal node corresponding to idx.
	 * @throws IndexOutOfBoundsException
	 *             if idx is not between lower and upper, inclusive.
	 */
	private Node<AnyType> getNode(int idx, int lower, int upper) {
		Node<AnyType> p;

		if (idx < lower || idx > upper)
			throw new IndexOutOfBoundsException("getNode index: " + idx
					+ "; size: " + size());

		if (idx < size() / 2) { // Search through list from the beginning
			p = beginMarker.next;
			for (int i = 0; i < idx; i++)
				p = p.next;
		} else { // search through the list from the end
			p = endMarker;
			for (int i = size(); i > idx; i--)
				p = p.prev;
		}

		return p;
	}

	/**
	 * Gets the Node at position idx, which must range from 0 to size( ) - 1.
	 * 
	 * @param idx
	 *            index to search at.
	 * @return internal node corresponding to idx.
	 * @throws IndexOutOfBoundsException
	 *             if idx is not between 0 and size( ) - 1, inclusive.
	 */
	private Node<AnyType> getNode(int idx) {
		return getNode(idx, 0, size() - 1);
	}

	/**
	 * Returns the item at position idx.
	 * 
	 * @param idx
	 *            the index to search in.
	 * @throws IndexOutOfBoundsException
	 *             if index is out of range.
	 */
	public AnyType get(int idx) {
		return getNode(idx).data;
	}

	/**
	 * Changes the item at position idx.
	 * 
	 * @param idx
	 *            the index to change.
	 * @param newVal
	 *            the new value.
	 * @return the old value.
	 * @throws IndexOutOfBoundsException
	 *             if index is out of range.
	 */
	public AnyType set(int idx, AnyType newVal) {
		Node<AnyType> p = getNode(idx);
		AnyType oldVal = p.data;

		p.data = newVal;
		return oldVal;
	}

	/**
	 * Adds an item to this collection, at specified position p. Items at or
	 * after that position are slid one position higher.
	 * 
	 * @param p
	 *            Node to add before.
	 * @param x
	 *            any object.
	 * @throws IndexOutOfBoundsException
	 *             if idx is not between 0 and size(), inclusive.
	 */
	private void addBefore(Node<AnyType> p, AnyType x) {
		Node<AnyType> newNode = new Node<>(x, p.prev, p);
		newNode.prev.next = newNode;
		p.prev = newNode;
		theSize++;
		modCount++;
	}

	/**
	 * Adds an item to this collection, at specified position. Items at or after
	 * that position are slid one position higher.
	 * 
	 * @param x
	 *            any object.
	 * @param idx
	 *            position to add at.
	 * @throws IndexOutOfBoundsException
	 *             if idx is not between 0 and size(), inclusive.
	 */
	public void add(int idx, AnyType x) {
		addBefore(getNode(idx, 0, size()), x);
	}

	/**
	 * Adds an item to this collection, at the end.
	 * 
	 * @param x
	 *            any object.
	 * @return true.
	 */
	public boolean add(AnyType x) {
		add(size(), x);
		return true;
	}

	/**
	 * Removes the object contained in Node p.
	 * 
	 * @param p
	 *            the Node containing the object.
	 * @return the item was removed from the collection.
	 */
	private AnyType remove(Node<AnyType> p) {
		p.next.prev = p.prev;
		p.prev.next = p.next;
		theSize--;
		modCount++;

		return p.data;
	}

	/**
	 * Removes an item from this collection.
	 * 
	 * @param idx
	 *            the index of the object.
	 * @return the item was removed from the collection.
	 */
	public AnyType remove(int idx) {
		return remove(getNode(idx));
	}

	/**
	 * Returns a String representation of this collection.
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder("[ ");

		for (AnyType x : this)
			sb.append(x + " ");
		sb.append("]");

		return new String(sb);
	}

	/**
	 * Obtains an Iterator object used to traverse the collection.
	 * 
	 * @return an iterator positioned prior to the first element.
	 */
	public java.util.Iterator<AnyType> iterator() {
		return new LinkedListIterator();
	}

	/********* ADD YOUR SOLUTIONS HERE *****************/

	/**
	 * This function looks for an object in a linked list
	 * and returns the index that it is located at. 
	 * 
	 * @param o Data object to search for. 
	 * @return Index of o
	 */
	public int indexOf(Object o) {

		int idx = 0;
		
		// Search through the linked list for object o. 
		for (AnyType n : this) {
			if (n.equals(o)) {
				return idx;
			}
			idx++;
		}
		
		// Return -1 if it's not found. 
		return -1;
	}
	/**
	 * Reverses the order of a linked list in linear time. 
	 * 
	 * To make this function operate in constant time, reversing the list 
	 * must not depend on the length of the list. The only way this can happen 
	 * is if the direction of prev and next is not set in stone. A flag can exist 
	 * that if true makes prev and next point one way, and if false makes them 
	 * point in the opposite way. Changing the direction they point in is done by
	 * changing the definition of 'prev' to mean 'next' and vice verse. Therefore, 
	 * no matter the size of the list, only one operation is performed depending on 
	 * the boolean value of the flag. 
	 * 
	 */
	public void reverse() {
		
		// Reverse the direction of endMarker.
		Node<AnyType> p = endMarker.prev;
		Node<AnyType> tmp = null;
		endMarker.next = p;
		endMarker.prev = null;
	
		// Reverse the direction of prev and next for all the nodes.
		for (int i = size(); i > 0; i--) {
			tmp = p.prev;
			p.prev = p.next;
			p.next = tmp;
			p = p.next;

		}
		
		// Reverse the direction of begin Marker.
		beginMarker.prev = beginMarker.next;
		beginMarker.prev = null;
		
		// Switch endMarker and beginMarker. 
		tmp = endMarker;
		endMarker = beginMarker;
		beginMarker = tmp;		

	}
	
	/**
	 * This method removes the copies of any elements that have
	 * one or more duplicate data entries. 
	 * 
	 * The running time of this function is O(N^2), which is quadratic. 
	 * The inner while loop runs (p-1) times while the outer while loop 
	 * run p times. Their product results in p^2 as the largest order of 
	 * magnitude. 
	 * If the list is sorted, the algorithm would only need to look next 
	 * to the data entry and delete everything until it reaches a data 
	 * entry that is different than the current one. This would only require
	 * one loop whose stopping condition would be finding a value different
	 * than the current one. Thus, with only one loop this algorithm would
	 * be linear time O(N). 
	 */
	public void removeDuplicates(){
		Node<AnyType> p = beginMarker;
		Node<AnyType> j = beginMarker.next;
		while(p.next != endMarker){
			j = p.next.next;
			p = p.next;
			
			// Search for the data entry of p in all the nodes after it. 
			while(j != endMarker){
				if (p.data.equals(j.data)) {
					remove(j);
				}
				j = j.next;
			}
			
		}
		
	}
	/**
	 * This function takes two linked lists and inserts one into the other
	 * by every other element, leaving the remaining elements of the longer list
	 * at the end. 
	 * 
	 * @param other Another linked list to interleave with the current one. 
	 * 
	 * The running time of this algorithm is O(N) and it depends on the size of the
	 * 'other' list. The function will keep running until all the elements of other 
	 * list are added to the list in question. 
	 */
	public void interleave (MyLinkedList<AnyType> other) {
		Node<AnyType> n1 = beginMarker.next.next;
		Node<AnyType> n2 = other.beginMarker.next;
		
		// Add elements to the list until that list runs out of elements.
		for (int i = 0; i < other.size() && n1.data != null; i++){
			addBefore(n1, n2.data);
			n1 = n1.next;
			n2 = n2.next;
			
		}
		
		// Then append the rest of the elements of the other list. 
		while (n2.data != null) {
			add(n2.data);
			n2 = n2.next;
		}
	}

	/**
	 * This is the implementation of the LinkedListIterator. It maintains a
	 * notion of a current position and of course the implicit reference to the
	 * MyLinkedList.
	 */
	private class LinkedListIterator implements java.util.Iterator<AnyType> {
		private Node<AnyType> current = beginMarker.next;
		private int expectedModCount = modCount;
		private boolean okToRemove = false;

		public boolean hasNext() {
			return current != endMarker;
		}

		public AnyType next() {
			if (modCount != expectedModCount)
				throw new java.util.ConcurrentModificationException();
			if (!hasNext())
				throw new java.util.NoSuchElementException();

			AnyType nextItem = current.data;
			current = current.next;
			okToRemove = true;
			return nextItem;
		}

		public void remove() {
			if (modCount != expectedModCount)
				throw new java.util.ConcurrentModificationException();
			if (!okToRemove)
				throw new IllegalStateException();

			MyLinkedList.this.remove(current.prev);
			expectedModCount++;
			okToRemove = false;
		}
	}

	/**
	 * Test the linked list.
	 */
	 public static void main( String [ ] args ) {
	        MyLinkedList<Integer> lst = new MyLinkedList<>( );

	        for( int i = 0; i < 10; i++ )
	                lst.add( i );
	        for( int i = 20; i < 30; i++ )
	                lst.add( 0, i );

	        lst.remove( 0 );
	        lst.remove( lst.size( ) - 1 );
	        MyLinkedList<Integer> list = new MyLinkedList<>( );
	        for( int i = 0; i <10; i++){
	        	list.add(i);
	        }
	        lst.interleave(list);

	        System.out.println( lst );

	    }
}
