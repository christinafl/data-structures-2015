package HW4;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * 
 * @author Christina Floristean
 * 
 *         A class that computes a running median for a sequence of numbers.
 *         The worst case time complexity is (N-k + 1) * k*logk where k is the size of the
 *         window and N is the number of elements. Each subarray takes k*logk
 *         time to sort and there will be a total of N-k+1 windows. 
 * 
 */
public class SlowMedian implements MovingMedian {
	double[] entries;

	// Constructor
	public SlowMedian() {
		entries = new double[100000];
	}

	/**
	 * Read the file into the double[].
	 */
	public void readFile(String filename) throws IOException {
		File text = new File(filename);

		// Read file.
		Scanner s = new Scanner(text);
		int index = 0;
		while (s.hasNextLine()) {
			entries[index] = Double.parseDouble(s.nextLine());
			index++;
		}
		s.close();
		
		// Trim array to correct size.
		entries = Arrays.copyOf(entries, index);

	}

	/**
	 * Find the median of a list of numbers.
	 */
	public double getMedian(double[] numbers, int start, int end) {
		double[] temp = new double[end - start + 1];
		int j = 0;
		
		// Copy values into the work array.
		for (int i = start; i <= end; i++) {
			temp[j] = numbers[i];
			j++;
		}
		
		// Sort the array and find the median.
		Arrays.sort(temp);
		return temp[temp.length / 2];
	}

	/**
	 * Find running medians.
	 */
	public double[] getMovingMedians(int window) {
		int start = 0;
		int end = window - 1;
		
		// Create array to contain all medians.
		double[] medians = new double[entries.length - window + 1];
		while (end < entries.length) {
			
			// Get median of each window. 
			medians[start] = getMedian(entries, start, end);
			start++;
			end++;
		}
		return medians;
	}

	public static void main(String[] args) throws IOException {
		SlowMedian s = new SlowMedian();
		s.readFile(args[0]);
		double[] med;
		med = s.getMovingMedians(5);

		// Print. 
		for (double d : med) {
			System.out.println(d);
		}

	}
}
