package HW4;

import java.lang.reflect.Array;

/**
 * 
 * @author Christina Floristean
 * 
 *         A class that carries out mergesort using a bottom-up implementation
 *         where pairs of elements combine into larger and larger subarrays.
 */
public class MergeSort<T> {

	/**
	 * Sorting method to determine how subarrays are merged.
	 * 
	 * @param a
	 */
	public static <T extends Comparable<T>> void sort(T[] a) {
		@SuppressWarnings("unchecked")
		// Array to temporarily work in.
		T[] b = (T[]) Array.newInstance(a[0].getClass(), a.length);
		int size = a.length;

		// Grow by powers of two until whole array is sorted.
		for (int j = 1; j < size; j = 2 * j) {
			for (int i = 0; i < size; i = i + 2 * j) {
				// Merge sections of a depending on the size of j (2^n).
				merge(a, b, i, min(i + j, size-1), min(i + 2 * j - 1, size-1));
			}

		}
	}

	/**
	 * Find the minimum of two values.
	 * 
	 * @param a
	 *            Value 1
	 * @param b
	 *            Value 2
	 * @return Minimum
	 */
	public static int min(int a, int b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}

	/**
	 * This is the merge method from the class slides.
	 * 
	 * @param a
	 * @param tmparray
	 * @param aCtr
	 * @param bCtr
	 * @param rightEnd
	 */
	private static <T extends Comparable<T>> void merge(T[] a, T[] tmparray,
			int aCtr, int bCtr, int rightEnd) {
		int leftEnd = bCtr-1;
		int tmpPos = aCtr;
		int numElements = rightEnd - aCtr + 1;
		// Main loop
		while (aCtr <= leftEnd && bCtr <= rightEnd)
			if (a[aCtr].compareTo(a[bCtr]) <= 0)
				tmparray[tmpPos++] = a[aCtr++];
			else
				tmparray[tmpPos++] = a[bCtr++];
		while (aCtr <= leftEnd)
			// Copy rest of first half
			tmparray[tmpPos++] = a[aCtr++];
		while (bCtr <= rightEnd)
			// Copy rest of right half
			tmparray[tmpPos++] = a[bCtr++];
		// Copy tmparray back
		for (int i = 0; i < numElements; i++, rightEnd--)
			a[rightEnd] = tmparray[rightEnd];
	}
	
	public static void main(String[] args){
		Double[] a = {1.0, 3.0, 2.0, 5.0, 2.0, 9.0, 6.0, 7.0, 4.0, 2.0};
		sort(a);
		for(int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}

}
