package HW4;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * 
 * @author Christina Floristean
 * 
 *         Class to keep track of the k largest elements in a stream.
 * 
 */
public class KBestCounter<T> {
	PriorityQueue<Comparable<T>> best;
	int size;

	// Constructor.
	public KBestCounter(int k) {
		best = new PriorityQueue<Comparable<T>>();
		size = k;
	}

	/**
	 * Determine if a number is within the k largest elements thus far.
	 * 
	 * @param x Number to consider.
	 */
	@SuppressWarnings("unchecked")
	public void count(T x) {
		
		// If there are not k elements, just add x.
		if (best.size() < size) {
			best.offer((Comparable<T>) x);
		} 
		else {
			
			// If this element is smaller than the root of the heap, 
			// put it in the heap and delete the root. 
			if ((best.peek()).compareTo(x) < 0) {
				best.remove();
				best.offer((Comparable<T>) x);
			}
		}

	}

	/**
	 * Remove items from the heap and put them in the list.
	 * 
	 * @return A list of the k largest numbers.
	 */
	public List<Integer> kbest() {
		LinkedList<Integer> list = new LinkedList<>();
		while (best.peek() != null) {
			list.push((Integer) best.remove());
		}
		return list;
	}

	public static void main(String[] args) {

		List<Integer> list = new LinkedList<>();

		for (int i = 1000; i > 0; i--) {
			list.add(i);
		}

		Iterator<Integer> stream = list.iterator();

		int k = 10;
		KBestCounter<Integer> counter = new KBestCounter<>(k);
		for (int i = 0; i < 100; i++) {
			counter.count(stream.next());
		}
		List<Integer> kbest = counter.kbest();
		// print k largest after 100 elements
		System.out.println(kbest);
		for (int i = 0; i < 100; i++) {
			counter.count(stream.next());
		}
		kbest = counter.kbest();
		// print k largest after 200 elements
		System.out.println(kbest);
	}
}
