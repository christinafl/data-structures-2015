package HW4;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * 
 * @author Christina Floristean
 * 
 *         A class that computes a running median for a sequence of numbers
 *         using quickselect.
 *         Worst case time complexity is O(k^2(N-k)) where k is the window size
 *         and N is the total amount of elements.This is because the worst case
 *         for quick select is O(N^2) when a bad pivot is chosen.Quickselect is
 *         only done for N-k elements.The best case for quick select is O(N), 
 *         which makes the best times for this program O(k^2(N-k)), since
 *         quick select is only done N-k times for k elements at a time.
 * 
 */
public class FastMedian implements MovingMedian {

	double[] entries;

	// Constructor
	public FastMedian() {
		entries = new double[100000];
	}

	/**
	 * Read the file into the double[].
	 */
	public void readFile(String filename) throws IOException {
		File text = new File(filename);

		// Read file.
		Scanner s = new Scanner(text);
		int index = 0;
		while (s.hasNextLine()) {
			entries[index] = Double.parseDouble(s.nextLine());
			index++;
		}
		s.close();
		
		// Trim array to correct size.
		entries = Arrays.copyOf(entries, index);

	}
	/**
	 * Use Median-of-Three method to find the pivot. 
	 * @param numbers 
	 * @param start
	 * @param end
	 * @return Median of this section of the data. 
	 */
	public double getPivot(double[] numbers, int start, int end) {
		if (numbers.length < 3) {
			return numbers[0];
		}
		int center = (start + end) / 2;
		
		// Order start and center of array. 
		if (numbers[start] > numbers[center])
			swap(numbers, start, center);
		
		// Order start and end of array.
		if (numbers[start] > numbers[end])
			swap(numbers, start, end);
		
		// Order center and end of array.
		if (numbers[center] > numbers[end])
			swap(numbers, center, end);

		// Return median.
		return numbers[center]; 
		
	}

	/**
	 * Return the median of a list of numbers.
	 */
	public double getMedian(double[] numbers, int start, int end) {
		int k = (end - start + 2) / 2;
		double[] temp = new double[end - start + 1];
		int j = 0;
		
		// Copy values into the work array.
		for (int i = start; i <= end; i++) {
			temp[j] = numbers[i];
			j++;
		}
		
		return quickSelect(temp, k);
				
	}
	
	/**
	 * Use quickselect to find the kth smallest number where k is the
	 * middle number. 
	 * @param numbers
	 * @param k Middle of array.
	 * @return Median of numbers[].
	 */
	public double quickSelect(double[] numbers, int k) {
		
		if (numbers.length == 1) {
			return numbers[0];
		}
		
		double pivot = getPivot(numbers, 0, numbers.length-1);
		LinkedList<Double> num1 = new LinkedList<Double>();
		LinkedList<Double> num2 = new LinkedList<Double>();
		int j = 0;
		// Move numbers in two two groups: smaller or larger than the pivot.
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] < pivot) {
				num1.add(numbers[i]);
			}
			else if (numbers[i] > pivot) {
				num2.add(numbers[i]);
			}
			
			else {
				j++;
				if (j > 1) {
					num1.add(numbers[i]);
				}
			}
		}
		
		
		// Recursively call quickSelect on the list of numbers that 
		// contains the middle number. 
		// The linked lists are turned to double[] to keep the parameters
		// consistent. This is pretty inefficient, but I did not want to 
		// use arrays because of the varying sizes. 
	
		if (k == num1.size()+1) {
			return pivot;
		}
		else if (k <= num1.size()) {
			pivot = quickSelect(getPrimitive(num1.toArray()),k);
			return pivot;
		}
		else {
			pivot = quickSelect(getPrimitive(num2.toArray()), k-num1.size());
			return pivot;
		}

	}
	
	/**
	 * This method turns and Object array into a double array.
	 * 
	 * @param a 
	 * @return double[]
	 */
	public double[] getPrimitive(Object[] a) {
		double[] b = new double[a.length];
		for (int i = 0; i < a.length; i++) {
			b[i] = (double)a[i];
		}
		return b;
	}

	/**
	 * Find running medians.
	 */
	public double[] getMovingMedians(int window) {
		int start = 0;
		int end = window - 1;
		
		// Create array to contain all medians.
		double[] medians = new double[entries.length - window + 1];
		while (end < entries.length) {
			
			// Get median of each window. 
			medians[start] = getMedian(entries, start, end);
			start++;
			end++;
		}
		return medians;
	}

	/**
	 * Switch two values in an array.
	 * 
	 * @param numbers
	 * @param a
	 * @param b
	 */
	public void swap(double[] numbers, int a, int b) {
		double temp = numbers[a];
		numbers[a] = numbers[b];
		numbers[b] = temp;
	}

	public static void main(String[] args) throws IOException {
		FastMedian s = new FastMedian();
		s.readFile(args[0]);
		double[] med;
		med = s.getMovingMedians(5);

		for (double d : med) {
			System.out.println(d);
		}
		

	}
}
