package HW3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * 
 * Make a map such that line numbers are attributed to words (keys) from a file.
 * 
 * @author Christina Floristean
 *
 */
public class CreateIndex {
	Map<String, LinkedList<Integer>> map;

	// Constructor.
	public CreateIndex() {
		map = new HashMap<String, LinkedList<Integer>>();
	}

	/**
	 *  Find value for a key.
	 * @param word Key in map.
	 * @return list of line numbers for a word.
	 */
	public LinkedList<Integer> getIndex(String word) {
		return map.get(word);
	}

	/**
	 *  Process input and create map.
	 * @param fileName Input file.
	 * @throws FileNotFoundException
	 */
	public void makeMap(String fileName) throws FileNotFoundException{
		File text = new File(fileName);
		
		// Read file.
		Scanner s = new Scanner(text);
		int lineNumber = 0;

		// Create map.
		while (s.hasNextLine()) {
			String line = s.nextLine();
			String[] words = line.split(" ");
			for (String word : words) {
				
				// If the key already exists, append line number to linked list.
				if (this.map.get(word) != null) {
					this.map.get(word).add(lineNumber);
				} 
				
				// If the key does not exist, create a new linked list and 
				// add the line number. Add this to the map.
				else {
					LinkedList<Integer> list = new LinkedList<Integer>();
					list.add(lineNumber);
					this.map.put(word, list);
				}

			}
			lineNumber++;
		}
		s.close();
	}
	
	public static void main(String[] args) throws FileNotFoundException{
		CreateIndex c = new CreateIndex();
		c.makeMap(args[0]);
		
		//Print.
		for(String key : c.map.keySet()) {
			System.out.print(key + "  ");
			for (int i : c.getIndex(key)) {
				System.out.print(i + "  ");
			}
			System.out.println();
		}

	}
}
