package HW3;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * 
 * Make a map such that words are attributed to line numbers (keys) from a file.
 * 
 * @author Christina Floristean
 *
 */
public class ReverseIndex {

	/**
	 *  Reverses keys and values and creates a new map.
	 * @param map Words-Line numbers map.
	 * @return map Line numbers-Words map.
	 */
	public static Map<Integer, LinkedList<String>> reverse(
			Map<String, LinkedList<Integer>> map) {
		
		Map<Integer, LinkedList<String>> rMap = new HashMap<Integer, LinkedList<String>>();
		
		// Traverse through every key.
		for (String key : map.keySet()) {
			
			// Traverse through all values of a key.
			for (int i = 0; i < map.get(key).size(); i++) {
				
				// Find new line number key.
				int lineNum = map.get(key).get(i);
				
				// Add word values to already existing keys.
				if (rMap.get(lineNum) != null) {
					rMap.get(lineNum).add(key);
				} 
				
				// Create new line number key and add value.
				else {
					LinkedList<String> list = new LinkedList<String>();
					list.add(key);
					rMap.put(lineNum, list);
				}

			}

		}
		return rMap;

	}

	public static void main(String[] args) throws FileNotFoundException {
		// Make map.
		CreateIndex r = new CreateIndex();
		r.makeMap(args[0]);

		Map<Integer, LinkedList<String>> revMap = reverse(r.map);
		
		// Print
		for (Integer key : revMap.keySet()) {
			System.out.print(key + "  ");
			for (String str : revMap.get(key)) {
				System.out.print(str + "  ");
			}
			System.out.println();
		}

	}
}
