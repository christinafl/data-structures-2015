package HW3;

import java.util.LinkedList;

/**
 * 
 * A separate chaining hash table that implements the Map interface.
 * 
 * @author Christina Floristean
 *
 * @param <K> Keys
 * @param <V> Values
 */
public class SeparateChainingMap<K extends Comparable<? super K>, V> implements
		Map<K, V> {
	private static int DEFAULT_TABLE_SIZE = 10;
	private LinkedList<Pair<K, V>>[] lists;
	private int currentSize;

	/**
	 *  Add a pair to the hash table.
	 */
	public void put(K key, V value) {
		Pair<K, V> n = new Pair<K, V>(key, value);
		LinkedList<Pair<K, V>> curList = lists[myhash(key)];
		
		// If the key is not already in the map, add it and check if
		// rehashing is needed.
		if (!curList.contains(key)) {
			curList.add(n);
			if (++currentSize / lists.length > 1)
				rehash();
		}
	}

	/**
	 *  Get the value associated with a key.
	 */
	public V get(K key) {
		LinkedList<Pair<K, V>> curList = lists[myhash(key)];
		for (Pair<K, V> n : curList) {
			if (n.key == key) {
				return n.value;
			}
		}
		return null;
	}


	// Constructor to create empty hash table.
	@SuppressWarnings("unchecked")
	public SeparateChainingMap() {
		lists = (LinkedList<Pair<K, V>>[]) new LinkedList[DEFAULT_TABLE_SIZE];
		for (int i = 0; i < lists.length; i++)
			lists[i] = new LinkedList<>();
	}

	/**
	 *  Rehash when load factor is greater than 1.
	 */
	@SuppressWarnings("unchecked")
	private void rehash() {
		LinkedList<Pair<K, V>>[] old = lists;
		// Create new double-sized table
		lists = (LinkedList<Pair<K, V>>[]) new LinkedList[2 * lists.length];
		for (int j = 0; j < lists.length; j++)
			lists[j] = new LinkedList<>();
		// Copy values into the new table.
		currentSize = 0;
		for (int i = 0; i < old.length; i++)
			for (Pair<K, V> item : old[i])
				put(item.key, item.value);

	}

	/**
	 *  Get hash value for a key.
	 * @param x Key
	 * @return Hash value of x
	 */
	private int myhash(K x) {
		int hashVal = x.hashCode();
		
		// Normalize value.
		hashVal %= lists.length;
		if (hashVal < 0)
			hashVal += lists.length;
		return hashVal;
	}

}
