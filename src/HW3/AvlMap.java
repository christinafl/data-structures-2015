package HW3;

/**
 * 
 * @author Christina Floristean
 *
 * AVL tree that implements the Map interface.
 *
 * @param <K> Keys
 * @param <V> Values
 */
public class AvlMap<K extends Comparable<? super K>, V> implements Map<K, V> {
	private AvlNode<K, V> root;
	private static final int ALLOWED_IMBALANCE = 1;

	// Constructor.
	public AvlMap() {
		root = null;
	}

	/**
	 * 
	 * @author Christina Floristean
	 *
	 * Node class for the AVL tree.
	 *
	 * @param <K> Key
	 * @param <V> Value
	 */
	private static class AvlNode<K extends Comparable<? super K>, V> {
		// Constructor.
		public AvlNode(Pair<K, V> theElement, AvlNode<K, V> lt, AvlNode<K, V> rt) {
			element = theElement;
			left = lt;
			right = rt;
			height = 0;
		}

		Pair<K, V> element; // The data in the node
		AvlNode<K, V> left; // Left child
		AvlNode<K, V> right; // Right child
		int height; // Height
	}

	/**
	 * Mutator method to add value to map.
	 */
	public void put(K key, V value) {
		root = put(root, new Pair<K, V>(key, value));
	}

	/**
	 * Add a node to the AvlMap.
	 * @param r Parent Node.
	 * @param p Key-value pair.
	 * @return AvlNode 
	 */
	private AvlNode<K, V> put(AvlNode<K, V> r, Pair<K, V> p) {
		
		// Map is empty.
		if (r == null) {
			return new AvlNode<K, V>(p, null, null);
		}
		int compareResult = r.element.compareTo(p);
		
		// Recursively move down the map to find the correct location.
		if (compareResult < 0) {
			r.left = put(r.left, p);
		}
		else if (compareResult > 0) {
			r.right = put(r.right, p);
		}
		else{
			;
		}// Duplicate; do nothing
		return balance(r);
	}

	/**
	 * Obtain the value of a key.
	 */
	public V get(K key) {
		AvlNode<K, V> t = root;
		
		// Search left or right to traverse the tree and find the value.
		while (t != null) {
			int compareResult = t.element.compareTo(new Pair<K, V>(key, null));

			if (compareResult < 0)
				t = t.left;
			else if (compareResult > 0)
				t = t.right;
			
			// Match.
			else
				return t.element.value; 
		}

		// No match.
		return null; 
	}

	/**
	 * Balance the Avl Tree
	 * Code provided in assignment.
	 */
	private AvlNode<K, V> balance(AvlNode<K, V> t) {
		if (t == null)
			return t;

		if (height(t.left) - height(t.right) > ALLOWED_IMBALANCE)
			if (height(t.left.left) >= height(t.left.right))
				t = rotateWithLeftChild(t);
			else
				t = doubleWithLeftChild(t);
		else if (height(t.right) - height(t.left) > ALLOWED_IMBALANCE)
			if (height(t.right.right) >= height(t.right.left))
				t = rotateWithRightChild(t);
			else
				t = doubleWithRightChild(t);

		t.height = Math.max(height(t.left), height(t.right)) + 1;
		return t;
	}

	/** 
	 * Find the height of the tree.
	 * Code provided in assignment.
	 */
	private int height(AvlNode<K, V> t) {
		return t == null ? -1 : t.height;
	}
	
	/**
	 * Rotation for balance.
	 * Code provided in assignment.
	 */
	private AvlNode<K, V> rotateWithLeftChild(AvlNode<K, V> k2) {
		AvlNode<K, V> k1 = k2.left;
		k2.left = k1.right;
		k1.right = k2;
		k2.height = Math.max(height(k2.left), height(k2.right)) + 1;
		k1.height = Math.max(height(k1.left), k2.height) + 1;
		return k1;
	}

	/**
	 *  Rotation for balance.
	 *  Code provided in assignment.
	 */
	private AvlNode<K, V> rotateWithRightChild(AvlNode<K, V> k1) {
		AvlNode<K, V> k2 = k1.right;
		k1.right = k2.left;
		k2.left = k1;
		k1.height = Math.max(height(k1.left), height(k1.right)) + 1;
		k2.height = Math.max(height(k2.right), k1.height) + 1;
		return k2;
	}

	/**
	 *  Rotation for balance.
	 *  Code provided in assignment.
	 */
	private AvlNode<K, V> doubleWithLeftChild(AvlNode<K, V> k3) {
		k3.left = rotateWithRightChild(k3.left);
		return rotateWithLeftChild(k3);
	}
	
	/**
	 *  Rotation for balance.
	 *  Code provided in assignment.
	 */
	private AvlNode<K, V> doubleWithRightChild(AvlNode<K, V> k1) {
		k1.right = rotateWithLeftChild(k1.right);
		return rotateWithRightChild(k1);
	}

	/**
	 *  Check if the map is empty.
	 * @return True is empty, False contains nodes.
	 */
	public boolean isEmpty() {
		return root == null;
	}

	/**
	 *  Check if tree is balanced.
	 *  Code provided in assignment.
	 */
	public void checkBalance() {
		checkBalance(root);
	}


	/**
	 *  Check if tree is balanced.
	 *  Code provided in assignment.
	 */
	private int checkBalance(AvlNode<K, V> t) {
		if (t == null)
			return -1;

		if (t != null) {
			int hl = checkBalance(t.left);
			int hr = checkBalance(t.right);
			if (Math.abs(height(t.left) - height(t.right)) > 1
					|| height(t.left) != hl || height(t.right) != hr)
				System.out.println("OOPS!!");
		}

		return height(t);
	}

	/**
	 *  Print the tree.
	 *  Code provided in assignment.
	 */
	public void printTree() {
		if (isEmpty())
			System.out.println("Empty tree");
		else
			printTree(root);
	}
	
	/**
	 *  Print the tree.
	 *  Code provided in assignment.
	 */
	private void printTree(AvlNode<K, V> t) {
		if (t != null) {
			printTree(t.left);
			System.out.println(t.element.key + " - " + t.element.value);
			printTree(t.right);
		}
	}
}
