package HW2;
import java.util.Stack;

// Part a: When letters go into the inbox stack, they come out in the reverse order. 
// By putting them into the outbox stack, they are then reversed again and therefore
// in the correct order. 

// Part b: Enqueue runs in constant time, since you just do one push. The order is O(1).
// Dequeue, however, is linear. If you find anything in the outbox, you do one pop, which 
// is one step. If you are moving things to outbox, this will take n steps depending on
// the number of items in inbox. If you enqueue n things, the first dequeue will take n+1
// to move the stack and take one out. Then the next n-1 dequeues will each take one step.
// Therefore, the worst case running time is 2n, or O(n);

// A queue implemented as two stacks.
public class TwoStackQueue<T> implements Queue<T>{
	private Stack<T> inbox;
	private Stack<T> outbox;
	
	// Constructor. 
	public TwoStackQueue() {
		inbox = new Stack<T>();
		outbox = new Stack<T>();	
	}
	
	// Add to the inbox.
	public void enqueue(T x){
		inbox.push(x);
	}
	
	// Retrieve from the outbox.
	public T dequeue() {
		if (outbox.empty()){
			
			// Don't bother searching if there isn't anything in the inbox.
			if (inbox.empty()){
				return null;
			}
			
			// Move from inbox to outbox.
			while (!inbox.empty()){
				outbox.push(inbox.pop());
			}
		}
		return outbox.pop();
	}
	
}

