package HW2;

import java.util.Scanner;
import java.util.Stack;

/**
 * 
 * @author Christina Floristean
 *
 * Creates a binary expression tree. When an expression is entered in postfix notation, 
 * it has the ability to transform it to prefix and infix, as well as evalutate
 * the expression.
 * 
 */
public class ExpressionTree<T> extends BinaryTree<T>{
	
	// Once the tree is built, this makes sure that the top node of the 
	// tree is always known. 
	final BinaryNode<T> topRoot;
	
	// Constructor.
	public ExpressionTree () {
		super();
		topRoot = null;
	}
	
	// Constructor.
	public ExpressionTree(BinaryNode<T> rootNode) {
        root = rootNode;
        topRoot = rootNode;
        
    }
	
	// Creates an binary expression tree using the standard API stack. 
	public static ExpressionTree<Character> makeExpressionTree(String input){
		Stack<ExpressionTree<Character>> s = new Stack<ExpressionTree<Character>>();
		
		// Break the expression into separate characters.
		char[] nums = input.toCharArray();
		for(int i = 0; i < nums.length; i++) {
			char c = nums[i];
			
			// Avoid taking in anything that isn't a digit or an operator.
			if (Character.isDigit(nums[i])) {
				s.push(btree(c));
			}
			
			// If an operator is found, pop the last two numbers in the stack and create a 
			// parent node with the operator and child nodes for the numbers.
			else if (isOperator(c)) {
				ExpressionTree<Character> r = s.pop();
				ExpressionTree<Character> l = s.pop();
				s.push(btree(c, l, r));
			}		
			
		}
		
		// Return the expression tree.
		return s.pop();		
		
	}
	
	// Used the same method from BinaryTree.
	// Could not inherit because it is a static method.
	public static <T> ExpressionTree<T> btree(T theItem) {
        return new ExpressionTree<T>(new BinaryNode<T>(theItem, null, null ));
    }
	
	// Used the same method from BinaryTree.
	// Could not inherit because it is a static method.
	public static <T> ExpressionTree<T> btree(T theItem, ExpressionTree<T> t1, ExpressionTree<T> t2) {
        BinaryNode<T> newRoot = new BinaryNode<T>(theItem, t1.root, t2.root);
        return new ExpressionTree<T>(newRoot);
    }
	
	// Checks to see if the character is an operator.
	public static boolean isOperator(char a) {
		return a == '+' || a == '-' || a == '*' || a == '/';
	} 
	
	// If the root changes through traversal, it is 
	// set back to the top root.
	public void rootReset(){
		root = topRoot;
	}
	
	// Transforms a postfix expression to prefix.
	public String toPrefix() {
		String exp = "";
		
		// Keeps track of the root in the level above
		// the nodes in question. 
		BinaryNode<T> tracker = root;
		if (root != null) {
			exp += root.data;
			
			// Recursively go the the left.
			root = tracker.left;
			exp += toPrefix();
			
			// Recursively go to the right.
			root = tracker.right;
			exp += toPrefix();
		}
		return exp;
	}

	// Changes a postfix expression to infix.
	public String toInfix() {
		String exp = "";
		
		// Keeps track of the root in the level above
		// the nodes in question. 
		BinaryNode<T> tracker = root;
		if (root != null) {
			
			// If an operator is found, add an open parenthesis.
			if(isOperator((Character)root.data)) {
				exp += "(";	
			}	
			
			// Recursively go left.
			root = tracker.left;
			exp += toInfix();
			
			exp += tracker.data;
			
			// Recursively go right.
			root = tracker.right;
			exp += toInfix();

			// Close the parenthesis around the expression.
			if (isOperator((Character)tracker.data)) {
				exp += ")";	
			}
			
			
		}
		return exp;
	}
	
	// Evaluate the expression.
	public float evaluate() {
		float result = 0;
		
		// Keeps track of the root in the level above
		// the nodes in question.
		BinaryNode<T> tracker = root;
		if (root != null) {
			
			// Change digit characters to actual digits.
			if (Character.isDigit((Character)root.data)) {
				char c = (Character)root.data;
				return (float)c - '0';
			}
			
			// Recursive section for addition.
			if ((Character)root.data == '+') {
				root = tracker.right;
				float k = evaluate();
				
				root = tracker.left;
				float j = evaluate();
				result += (j + k);
			}
			
			// Recursive section for subtraction.
			if ((Character)root.data == '-') {
				root = tracker.right;
				float k = evaluate();
				
				root = tracker.left;
				float j = evaluate();
				result += (j - k);	
			}
			
			// Recursive section for multiplication.
			if ((Character)root.data == '*') {
				root = tracker.right;
				float k = evaluate();
				
				root = tracker.left;
				float j = evaluate();
				result += (j * k);
			}
			
			// Recursive section for division.
			if ((Character)root.data == '/') {
				root = tracker.right;
				float k = evaluate();
				
				root = tracker.left;
				float j = evaluate();
				result += (j / k);
			}

		}
		return result;
	}
	
	
	
	public static void main(String[] args) {
		System.out.println("Enter a postfix expression: ");
		
		// Input.
		Scanner s = new Scanner(System.in);
		String exp = s.nextLine();
		s.close();
		
		ExpressionTree<Character> e = makeExpressionTree(exp);
		System.out.println("Prefix:" + e.toPrefix());
		e.rootReset();
		String inf = e.toInfix();
		
		// Remove the extra parenthesis around the whole expression.
		System.out.println("Infix: " + inf.substring(1,inf.length()-1));
		e.rootReset();
		System.out.println("Result: " + e.evaluate());
	}
}
