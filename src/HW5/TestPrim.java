package HW5;

import java.io.FileNotFoundException;

/**
 * Test the minimum spanning tree algorithm and display the results. 
 *
 */
public class TestPrim {

	public static void main(String[] args)throws FileNotFoundException{
		Graph g = MapReader.readGraph(args[0], args[1]);
		Graph h = g.getMinimumSpanningTree(args[2]);

		// Compute cost of shortest path.
		double totalCost = 0;
		for (Vertex v : h.getVertices()) {
			if (!v.getEdges().isEmpty()) {
				for (Edge e : v.getEdges()) {
					totalCost += e.cost;
				}

			}
		}

		System.out.println("The cost of the shortest path is " + totalCost + ". ");
		
	    DisplayGraph display = new DisplayGraph(h);
	    display.setVisible(true);
	}
}
