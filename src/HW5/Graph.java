package HW5;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class Graph {

	// Keep a fast index to nodes in the map
	protected Map<String, Vertex> vertices;

	/**
	 * Construct an empty Graph.
	 */
	public Graph() {
		vertices = new HashMap<String, Vertex>();
	}

	public void addVertex(String name) {
		Vertex v = new Vertex(name);
		addVertex(v);
	}

	public void addVertex(Vertex v) {
		if (vertices.containsKey(v.name))
			throw new IllegalArgumentException(
					"Cannot create new vertex with existing name.");
		vertices.put(v.name, v);
	}

	public Collection<Vertex> getVertices() {
		return vertices.values();
	}

	public Vertex getVertex(String s) {
		return vertices.get(s);
	}

	/**
	 * Add a new edge from u to v. Create new nodes if these nodes don't exist
	 * yet. This method permits adding multiple edges between the same nodes.
	 * 
	 * @param u
	 *            the source vertex.
	 * @param w
	 *            the target vertex.
	 */
	public void addEdge(String nameU, String nameV, Double cost) {
		if (!vertices.containsKey(nameU))
			addVertex(nameU);
		if (!vertices.containsKey(nameV))
			addVertex(nameV);
		Vertex sourceVertex = vertices.get(nameU);
		Vertex targetVertex = vertices.get(nameV);
		Edge newEdge = new Edge(sourceVertex, targetVertex, cost);
		sourceVertex.addEdge(newEdge);
	}

	/**
	 * Add a new edge from u to v. Create new nodes if these nodes don't exist
	 * yet. This method permits adding multiple edges between the same nodes.
	 * 
	 * @param u
	 *            unique name of the first vertex.
	 * @param w
	 *            unique name of the second vertex.
	 */
	public void addEdge(String nameU, String nameV) {
		addEdge(nameU, nameV, 1.0);
	}

	/**
	 * Creates two edges, essentially making a double-sided arrow.
	 * 
	 * @param s
	 *            Vertex 1
	 * @param t
	 *            Vertex 2
	 * @param cost
	 *            Weight of edge
	 */
	public void addUndirectedEdge(String s, String t, Double cost) {
		addEdge(s, t, cost);
		addEdge(t, s, cost);
	}

	/**
     * 
     */
	public void printAdjacencyList() {
		for (String u : vertices.keySet()) {
			StringBuilder sb = new StringBuilder();
			sb.append(u);
			sb.append(" -> [ ");
			for (Edge e : vertices.get(u).getEdges()) {
				sb.append(e.targetVertex.name);
				sb.append("(");
				sb.append(e.cost);
				sb.append(") ");
			}
			sb.append("]");
			System.out.println(sb.toString());
		}
	}

	/**
	 * Finds the Euclidean distance between two points, which is the weight of
	 * the edge.
	 */
	public void computeEuclideanCosts() {
		for (Vertex v : vertices.values()) {
			for (Edge e : v.getEdges()) {
				double sum = Math.pow(
						e.targetVertex.posX - e.sourceVertex.posX, 2)
						+ Math.pow(e.targetVertex.posY - e.sourceVertex.posY, 2);
				e.cost = Math.sqrt(sum);
			}
		}
	}

	/**
	 * Uses breadth-first search to find the shortest path between two vertices.
	 * Creates graph of all of these points, but only displays the edges along
	 * the path.
	 * 
	 * @param s
	 *            Vertex 1
	 * @param t
	 *            Vertex 2
	 * @return Graph of shortest path
	 */
	public Graph getUnweightedShortestPath(String s, String t) {
		Graph graph = new Graph();

		// Breadth-first search
		doBfs(s);

		// Put every vertex in the new graph.
		for (Vertex v : getVertices()) {
			if (graph.getVertex(v.name) == null) {
				graph.addVertex(new Vertex(v.name, v.posX, v.posY));
			}
		}

		// Work backwards from the destination until the starting point
		// is reached. Weights are all equal.
		Vertex cur = getVertex(t);
		while (cur.prev != null) {
			graph.addUndirectedEdge(cur.prev.name, cur.name, 1.0);
			cur = cur.prev;
		}

		return graph;
	}

	/**
	 * Breadth-first search.
	 * 
	 * @param s
	 *            Starting vertex.
	 */
	public void doBfs(String s) {

		// Create queue and put starting vertex inside it.
		Queue<Vertex> q = new LinkedList<Vertex>();
		getVertex(s).visited = true;
		q.add(getVertex(s));
		while (!q.isEmpty()) {
			Vertex cur = q.remove();

			// For every adjacent vertex.
			for (Edge e : cur.getEdges()) {

				// If the item has not already been visited.
				if (!e.targetVertex.visited) {

					// Add it to the queue, link the two vertices, and
					// update member variables of the adjacent vertex.
					q.add(e.targetVertex);
					e.targetVertex.prev = cur;
					e.targetVertex.cost = cur.cost + 1;
					e.targetVertex.visited = true;
				}
			}

		}

	}

	/**
	 * Uses Dijkstra's algorithm to find the shortest path between two vertices.
	 * Creates graph of all of these points, but only displays the edges along
	 * the path.Edges are weighted.
	 * 
	 * @param s
	 *            Vertex 1
	 * @param t
	 *            Vertex 2
	 * @return Graph of shortest path.
	 */
	public Graph getWeightedShortestPath(String s, String t) {
		Graph graph = new Graph();

		// Add weights and traverse the graph.
		computeEuclideanCosts();
		doDijkstra(s);

		// Add all vertices to the new graph.
		for (Vertex v : getVertices()) {
			if (graph.getVertex(v.name) == null) {
				graph.addVertex(new Vertex(v.name, v.posX, v.posY));
			}
		}

		// Work backwards from the destination until the starting point
		// is reached.
		Vertex cur = getVertex(t);
		while (cur.prev != null) {
			double c = 0;
			for (Edge e : cur.getEdges()) {

				// Find the weight of the edge between the current vertex and
				// the one pointing to it, then break to stop searching.
				if (e.targetVertex == cur.prev) {
					c = e.cost;
					break;
				}
			}

			// Add to the new graph.
			graph.addEdge(cur.prev.name, cur.name, c);
			cur = cur.prev;
		}

		return graph;

	}

	/**
	 * Perform Dijkstra's traversal algorithm.
	 * 
	 * @param s
	 *            Starting vertex.
	 */
	public void doDijkstra(String s) {
		PriorityQueue<Vertex> queue = new PriorityQueue<Vertex>();

		// Set each vertex to infinite cost and not visited.
		for (Vertex v : getVertices()) {
			v.cost = Integer.MAX_VALUE;
			v.visited = false;
		}

		// Add the starting point to the queue.
		getVertex(s).visited = true;
		getVertex(s).cost = 0;
		queue.add(getVertex(s));
		while (!queue.isEmpty()) {

			// Visit the minimum value
			Vertex cur = queue.poll();
			cur.visited = true;

			// For every edge of every vertex.

			for (Edge e : cur.getEdges()) {

				// If the edge is adjacent and the vertex has not already
				// been visited.
				if (!e.targetVertex.visited) {

					// Change the infinite temporary value of v to
					// its true cost.
					if (cur.cost + e.cost < e.targetVertex.cost) {
						e.targetVertex.cost = cur.cost + e.cost;
						e.targetVertex.prev = cur;
					}
					queue.add(e.targetVertex);
				}
			}

		}

	}

	/**
	 * Use Prim's algorithm to get a minimum spanning tree.
	 * 
	 * @param s
	 *            Starting Vertex.
	 * @return Graph of minimum spanning tree.
	 */
	public Graph getMinimumSpanningTree(String s) {
		Graph graph = new Graph();

		// Add weights and perform Prim's algorithm.
		computeEuclideanCosts();
		doPrim(s);

		// Add all the vertices to the new graph.
		for (Vertex v : getVertices()) {
			if (graph.getVertex(v.name) == null) {
				graph.addVertex(new Vertex(v.name, v.posX, v.posY));
			}
		}

		// Go through each vertex and add the edge connecting
		// the previous node to the current one.
		for (Vertex v : getVertices()) {
			double c = 0;
			if (v.prev != null) {
				for (Edge e : v.getEdges()) {

					// Find weight of edge then stop searching.
					if (e.targetVertex == v.prev) {
						c = e.cost;
						break;
					}
				}
				graph.addEdge(v.prev.name, v.name, c);
			}

		}

		return graph;

	}

	/**
	 * Prim's algorithm to find a minimum spanning tree.
	 * 
	 * @param s
	 *            Starting vertex.
	 */
	public void doPrim(String s) {
		PriorityQueue<Vertex> queue = new PriorityQueue<Vertex>();

		// Set each vertex to infinite cost and not visited.
		for (Vertex v : getVertices()) {
			v.cost = Integer.MAX_VALUE;
			v.visited = false;
		}

		// Add the starting point to the queue.
		getVertex(s).visited = true;
		getVertex(s).cost = 0;
		queue.add(getVertex(s));
		while (!queue.isEmpty()) {

			// Visit the minimum value.
			Vertex cur = queue.poll();
			cur.visited = true;

			// For every edge of every vertex.

			for (Edge e : cur.getEdges()) {

				// If the edge is adjacent and the vertex has not already
				// been visited.
				if (!e.targetVertex.visited) {

					// Remove the temporary infinite value of v and
					// replace it with its actual cost.
					if (e.cost < e.targetVertex.cost) {
						e.targetVertex.cost = e.cost;
						e.targetVertex.prev = cur;
					}
					queue.add(e.targetVertex);
				}
			}

		}
	}

	public static void main(String[] args) {
		Graph g = new Graph();

		g.addVertex(new Vertex("v0", 0, 0));
		g.addVertex(new Vertex("v1", 0, 1));
		g.addVertex(new Vertex("v2", 1, 0));
		g.addVertex(new Vertex("v3", 1, 1));

		g.addEdge("v0", "v1");
		g.addEdge("v1", "v2");
		g.addEdge("v2", "v3");
		g.addEdge("v3", "v0");
		g.addEdge("v0", "v2");
		g.addEdge("v1", "v3");

		g.printAdjacencyList();

		DisplayGraph display = new DisplayGraph(g);
		display.setVisible(true);
	}

}
