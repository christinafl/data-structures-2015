package HW5;

import java.io.FileNotFoundException;

/**
 * Test Dijkstra's shortest path algorithm for weighted edges and display the
 * results.
 * 
 * The path from Miami to Vancouver is different when using the Breadth-First
 * Search algorithm.
 * 
 */
public class TestDijkstra {

	public static void main(String[] args) throws FileNotFoundException {
		Graph g = MapReader.readGraph(args[0], args[1]);
		Graph h = g.getWeightedShortestPath(args[2], args[3]);

		// Compute cost of shortest path.
		double totalCost = 0;
		for (Vertex v : h.getVertices()) {
			if (!v.getEdges().isEmpty()) {
				for (Edge e : v.getEdges()) {
					totalCost += e.cost;
				}

			}
		}

		System.out.println("The cost of the shortest path is " + totalCost + ". ");

		DisplayGraph display = new DisplayGraph(h);
		display.setVisible(true);
	}
}
