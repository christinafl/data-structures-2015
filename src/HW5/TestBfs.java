package HW5;

import java.io.FileNotFoundException;

/**
 * Test the Breadth-First Search algorithm to find the shortest path
 * and display the results.
 * 
 */
public class TestBfs {
	public static void main(String[] args)throws FileNotFoundException{
		Graph g = MapReader.readGraph(args[0], args[1]);
		Graph h = g.getUnweightedShortestPath(args[2], args[3]);
		
		// Compute cost of shortest path.
		double totalCost = 0;
		for(Vertex v : h.getVertices()) {
			if (!v.getEdges().isEmpty()) {
				for (Edge e : v.getEdges()) {
					totalCost += e.cost;
				}
				
			}
		}
		
		// Each undirected edge is counted twice so divide by 2 to avoid this.
		System.out.println("The cost of the shortest path is " + (totalCost / 2));
		
		
	    DisplayGraph display = new DisplayGraph(h);
	    display.setVisible(true);
	}
	

}
