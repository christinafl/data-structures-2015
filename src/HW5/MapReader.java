package HW5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MapReader {
	
	/**
	 * Create graph from two data files.
	 * 
	 * @param vertexfile File of vertices.
	 * @param edgefile File of edges.
	 * @return Graph of vertices connected by edges.
	 * @throws FileNotFoundException
	 */
	static Graph readGraph(String vertexfile, String edgefile) throws FileNotFoundException {
		Graph g = new Graph();
		File vertex = new File(vertexfile);
		File edge = new File(edgefile);
		Scanner v = new Scanner(vertex);
		Scanner e = new Scanner(edge);
		
		// Read in each vertex.
		while(v.hasNextLine()){
			
			// Split by comma to get data (name, x coordinate, y coordinate).
			String[] info = (v.nextLine()).split(",");
			if(g.getVertex(info[0]) == null){
				g.addVertex(new Vertex(info[0], Integer.parseInt(info[1]), Integer.parseInt(info[2])));
			}
			
		}
		v.close();
		
		// Read in each edge.
		while(e.hasNextLine()) {
			
			// Split by comma and add two vertices connected by edge of
			// cost 1.
			String[] data = (e.nextLine()).split(",");
			g.addUndirectedEdge(data[0], data[1], 1.0);
		}
		e.close();
		
		
		return g;
		
	}
	
	public static void main(String[] args) throws FileNotFoundException{
		Graph g = readGraph(args[0], args[1]);
		Graph h = g.getUnweightedShortestPath("SantaFe", "LosAngeles");
	    DisplayGraph display = new DisplayGraph(h);
	    display.setVisible(true);
		
		
	}
}
