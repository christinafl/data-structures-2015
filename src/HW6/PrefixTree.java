package HW6;

import java.util.LinkedList;
import java.util.List;

public class PrefixTree<T> {
	protected TreeNode<T> root;

	/**
	 * Represent a Prefix subtree.
	 */
	protected static class TreeNode<T> {

		public T data; // the data
		public List<TreeNode<T>> children;
		boolean endOfWord;

		/**
		 * Construct a new node.
		 */
		public TreeNode(T theData, boolean end) {
			data = theData;
			children = new LinkedList<TreeNode<T>>();
			endOfWord = end;

		}

		/**
		 * Add a child.
		 * 
		 * @param child
		 */
		public void addChild(TreeNode<T> child) {
			children.add(child);
		}

		/**
		 * Add a child at a particular index.
		 * 
		 * @param index
		 * @param child
		 * @throws IndexOutOfBoundsException
		 */
		public void addChild(int index, TreeNode<T> child)
				throws IndexOutOfBoundsException {

			children.add(index, child);

		}

	} // Nested class TreeNode ends here.

	/**
	 * Construct a new empty N-aryTree
	 */
	public PrefixTree() {
		root = new TreeNode<T>(null, false);
	}

	/**
	 * Add a word to the tree.
	 * 
	 * @param word
	 */
	@SuppressWarnings("unchecked")
	public void addWord(String word) {
		TreeNode<T> node = root;

		// Index for letter in word that we are looking to add.
		int idx = 0;

		// Index in the children list of a node.
		int j = 0;

		// Until there are no children left
		while (!node.children.isEmpty()) {
			TreeNode<T> t = node.children.get(j);

			// If node data matches the current letter in the word
			if ((Character) t.data == word.toCharArray()[idx]) {
				// Move to next letter.
				idx++;

				// Move on to this node and its children.
				node = t;

				// Beginning of new children list.
				j = 0;

			}

			// Increment j if characters don't match until it reaches the
			// end of the list of children.
			else if (j != node.children.size() - 1) {
				j++;
			}

			// When it does reach the end, get out of this loop.
			else {
				break;
			}
		}

		// Remaining letters not found in tree.
		String leftover = word.substring(idx);
		TreeNode<T> cur = node;
		int i = 0;
		boolean isEnd = false;
		for (Character c : leftover.toCharArray()) {
			if (i == leftover.toCharArray().length - 1) {
				isEnd = true;
			}

			// Make each remaining node a child of the preceding one.
			cur.addChild(new TreeNode<T>((T) c, isEnd));
			i++;
			cur = cur.children.get(cur.children.size() - 1);
		}
	}

	/**
	 * Find the prefix of a word in the tree.
	 * 
	 * @param prefix
	 * @return Tree beneath last letter in prefix.
	 */
	public PrefixTree<T> findPrefix(String prefix) {
		TreeNode<T> cur = root;

		// Index for current letter in word.
		int idx = 0;

		// Index in the children list of a node.
		int i = 0;

		// While there are children left
		while (!cur.children.isEmpty()) {
			TreeNode<T> t = cur.children.get(i);

			// If node data matches the current letter in the word
			if ((Character) t.data == prefix.toCharArray()[idx]) {
				// If we reached the end of the word, get out of loop.
				if (idx == prefix.toCharArray().length - 1) {
					cur = t;
					break;
				}

				// Move on to next letter and the next node.
				idx++;
				cur = t;
				i = 0;

			}

			// Increment i if characters don't match until it reaches the
			// end of the list of children.
			else if (i != cur.children.size() - 1) {
				i++;
			}

			// When it does reach the end and the rest of the word is not
			// in the tree, it means the prefix was not found. 
			else if (i == cur.children.size() - 1) {
				return null;
			}

		}

		// Create a new prefix tree starting with the last letter of the
		// prefix.
		PrefixTree<T> t = new PrefixTree<T>();
		List<TreeNode<T>> temp = new LinkedList<TreeNode<T>>();
		temp.add(cur);
		t.root.children = temp.get(0).children;
		t.root = cur;

		return t;
	}

	/**
	 * Check if a word is in the tree.
	 * 
	 * @param word
	 * @return boolean Found or not.
	 */
	public boolean contains(String word) {
		boolean contains = false;
		PrefixTree<T> t = findPrefix(word);

		// Not found.
		if (t == null) {
			return contains;
		}

		// If the prefix starts with an end-of-word letter, it is in the tree.
		// An addition condition that check if the letter equal the last letter
		// of the word is also there to make sure the whole word is there.
		// However, if we look at a work like casa and search for casablanca,
		// this
		// will not work. Since there is nothing pointing to the parent, we
		// cannot
		// check to see if the preceding letters are the same.
		else if (t.root.endOfWord
				&& (Character) t.root.data == word.charAt(word.length() - 1)) {
			contains = true;
		}
		return contains;

	}

	/**
	 * Rough printing function just to see if tree is shaped correctly. Used
	 * only for testing.
	 * 
	 * @param node
	 * @param s
	 */
	public void printSorted(TreeNode<T> node, Character s) {
		// For every child of the node
		for (int ch = 0; ch < node.children.size(); ch++) {
			TreeNode<T> child = node.children.get(ch);

			// Print the letter.
			if (child != null)
				System.out.println(s);

			// Recursively call function with child node.
			printSorted(child, (Character) child.data);
		}

		if (node.endOfWord) {
			System.out.print(s + " ");
		}
	}

	/**
	 * Find the prefix of a word in the tree.
	 * 
	 * @param prefix
	 * @return Tree beneath last letter in prefix.
	 */
	public PrefixTree<T> findClosestPrefix(String prefix) {
		TreeNode<T> cur = root;

		// Index for current letter in word.
		int idx = 0;

		// Index in the children list of a node.
		int i = 0;

		// While there are children left
		while (!cur.children.isEmpty()) {
			TreeNode<T> t = cur.children.get(i);

			// If node data matches the current letter in the word
			if ((Character) t.data == prefix.toCharArray()[idx]) {
				// If we reached the end of the word, get out of loop.
				if (idx == prefix.toCharArray().length - 1) {
					cur = t;
					break;
				}

				// Move on to next letter and the next node.
				idx++;
				cur = t;
				i = 0;

			}

			// Increment i if characters don't match until it reaches the
			// end of the list of children.
			else if (i != cur.children.size() - 1) {
				i++;
			}

			// When it does reach the end and the rest of the word is not
			// in the tree, get out of this loop.
			else if (i == cur.children.size() - 1) {
				break;
			}

		}

		// If the prefix does not exist return null.
		if (cur == root) {
			return null;
		}

		// Create a new prefix tree starting from the last letter found
		// in the tree.
		PrefixTree<T> t = new PrefixTree<T>();
		List<TreeNode<T>> temp = new LinkedList<TreeNode<T>>();
		temp.add(cur);
		t.root.children = temp.get(0).children;
		t.root = cur;

		return t;
	}

	// Main method for testing.
	public static void main(String[] args) {
		PrefixTree<Character> tree = new PrefixTree<Character>();
		tree.addWord("do");
		tree.addWord("dog");
		tree.addWord("doll");
		tree.addWord("dock");
		tree.addWord("cat");
		tree.printSorted(tree.root, '\0');
		System.out.println();
		tree.findPrefix("cat");
		boolean contains = tree.contains("brazil");
		System.out.println(contains);

	}

}
