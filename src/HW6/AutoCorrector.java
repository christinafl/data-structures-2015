package HW6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class AutoCorrector<T> {
	PrefixTree<T> tree;

	// Constructor. 
	public AutoCorrector() {
		tree = new PrefixTree<T>();
	}

	// Constructor given a filename. 
	public AutoCorrector(String filename) throws FileNotFoundException {
		tree = new PrefixTree<T>();
		
		 File file = new File(filename); Scanner s = new Scanner(file);
		 
		 // Add every word in file to tree. 
		 while (s.hasNext()) { tree.addWord(s.nextLine());
		 
		 } s.close();

	}

	/**
	 * Perform depth-first search for every node after a certain prefix. 
	 * 
	 * @param t
	 * @param prefix
	 * @return List of words with the same prefix. 
	 */
	public List<String> getWord(PrefixTree<T> t, String prefix) {
		Stack<PrefixTree.TreeNode<T>> s = new Stack<PrefixTree.TreeNode<T>>();
		
		// Visited nodes. 
		Set<PrefixTree.TreeNode<T>> visited = new HashSet<PrefixTree.TreeNode<T>>();
		
		List<String> words = new LinkedList<String>();
		String word = prefix;
		s.push(t.root);
		visited.add(t.root);
		while (!s.isEmpty()) {
			
			// Current node.
			PrefixTree.TreeNode<T> cur = (PrefixTree.TreeNode<T>) s.pop();
			for (PrefixTree.TreeNode<T> child : cur.children) {
				
				// Push the unvisited neighbors of the current node onto the
				// stack.
				if (!visited.contains(child)) {
					visited.add(child);
					s.push(child);
				}
			}
			
			// When end of a word is reached, add the letter and reset
			// word to the previous word (current prefix). 
			if (cur.endOfWord) {
				prefix = word;
				word += cur.data.toString();
				words.add(word);
				word = prefix;
			}
			// Visit node.
			else {
				word += cur.data.toString();
			}
		}
		return words;
	}

	/**
	 * Find minimum edit distance from words in the tree to word. 
	 * @param word
	 * @return String closest to word. 
	 */
	public String correct(String word) {
		List<String> words = new LinkedList<String>();
		boolean contains = tree.contains(word);
		
		// If it is in the tree, return it. 
		if (contains) {
			return word;
		} else {
			
			// Find sub-prefix tree of word. 
			PrefixTree<T> pref = tree.findClosestPrefix(word);
			
			if (pref == null) {
				return null;
			}
			PrefixTree.TreeNode<T> node = pref.root;
			
			// Get lists of words with the same prefix. 
			String c = word.substring(0, word.indexOf((Character) node.data));
			words = getWord(pref, c);

		}
		
		// Find and return minimum edit distance. 
		int minDistance = Integer.MAX_VALUE;
		String minWord = "";
		for (String w : words) {
			int distance = EditDistance.editDistance(w, word);
			if (distance < minDistance) {
				minDistance = distance;
				minWord = w;
			}

		}

		return minWord;

	}

	public static void main(String[] args) throws FileNotFoundException {
		AutoCorrector<String> auto = new AutoCorrector<String>(args[0]);
		Scanner s = new Scanner(System.in);
		System.out.print("Enter a word: ");
		while (s.hasNext()) {
			String entered = s.nextLine();
			String w = auto.correct(entered);
			
			if (w == null) {
				System.out.println("Warning: Unknown Word - " + entered);
			} else {
				System.out.println(w);
			}
			System.out.println();
			System.out.print("Enter a word: ");
		}
		s.close();
		
	}
}
