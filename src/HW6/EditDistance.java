package HW6;

public class EditDistance {
	
	/**
	 * Find the minimum edit distance from string s to string t. 
	 * 
	 * @param s
	 * @param t
	 * @return minimum distance
	 */
	public static int editDistance(String s, String t){ 
		int deletion;
		int insertion;
		int substitution;

		int[][] values = new int[s.length()][t.length()];
		
		// Initialize table. 
		for (int i = 0; i < s.length(); i++) {
			values[i][0] = i;
		}
		for (int j = 1; j < t.length();j++) {
			values[0][j] = j;
		}
		
		// For every letter of each word
		for(int i = 1;  i < s.length(); i++) {
			for (int j = 1; j < t.length(); j++) {
				
				// Calculate deletion, insertion, and substitution values using 
				// previous values in table. 
				deletion = values[i-1][j] + 1;
				insertion = values[i][j-1] + 1;
				substitution = values[i-1][j-1];
				
				// If letters are not the same, increment. 
				if (s.charAt(i) != t.charAt(j)) {
					substitution++;
				}
				
				// Input minimum of these operations. 
				values[i][j] = Math.min(deletion, Math.min(insertion, substitution));
			}
		}
		
		// Return minimum edit distance. 
		return values[s.length()-1][t.length()-1];
		
	}
	

}
